import { ForwardAppPage } from './app.po';

describe('forward-app App', () => {
  let page: ForwardAppPage;

  beforeEach(() => {
    page = new ForwardAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
