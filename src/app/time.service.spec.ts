import { TestBed, inject } from '@angular/core/testing';
import { TimeService } from './time.service';
import { WeatherService } from "./weather.service";

describe('TimeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimeService,
        { provide: WeatherService, useClass: MockWeatherService }
      ]
    });
  });

  it('should make weatherstring', inject([TimeService, WeatherService], (service: TimeService, weatherService: WeatherService) => {

    expect(weatherService.makeWeatherString()).toBeTruthy();
  }));
});

class MockWeatherService {

}
