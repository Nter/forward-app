import {Component, forwardRef, Inject} from '@angular/core';
import { WeatherService } from './weather.service';
import construct = Reflect.construct;
import { TimeService } from "./time.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [{ provide: TimeService, useExisting: forwardRef(() => TimeService) }],
})

export class AppComponent {
  title = 'app works!';
  wetterId: string;
  wetterStatus: string;
  wetterDatum: string;
  wochentag: string;

  constructor(timeService: TimeService, weatherService: WeatherService) {
    weatherService.makeWeatherString();
    this.wetterId = weatherService.getWeatherId();
    this.wetterStatus = weatherService.getWeatherStatus();
    this.wetterDatum= weatherService.getWeatherDate();
    this.wochentag = timeService.getTime();
  }
}
