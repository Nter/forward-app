import { BrowserModule } from '@angular/platform-browser';
import {forwardRef, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {WeatherService} from "./weather.service";
import {TimeService} from "./time.service";
/*import { WeatherService } from "./weather.service";
import { TimeService } from "./time.service";*/

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    /*{ provide: WeatherService, useExisting: forwardRef(() => TimeService) },
    { provide: TimeService, useExisting: forwardRef(() => WeatherService) }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
