import { forwardRef, Inject, Injectable } from '@angular/core';
import { WeatherService } from "./weather.service";

@Injectable()
export class TimeService {
  time: string;
  trash: string;

  constructor (@Inject(forwardRef(() => WeatherService))
               private weatherService: WeatherService) {
    weatherService.makeWeatherString();
  }

  getTime(){
    this.time = new Date().toString();
    return this.time;
  }
}
