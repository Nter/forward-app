import { TestBed, inject } from '@angular/core/testing';
import { WeatherService } from './weather.service';
import { TimeService } from "./time.service";

describe('WeatherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        WeatherService,

        { provide: TimeService, useClass: MockTimeService }
      ]
    })
  });

  it('should ...', inject([WeatherService, TimeService], (weatherService: WeatherService, timeService: TimeService) => {
    expect(timeService.time).toBeTruthy();
  }));
});

class MockTimeService {
  time: any;
  getTime() {
    this.time = 'Zeit hier einfügen';
  }
}
