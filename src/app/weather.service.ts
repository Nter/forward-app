import { Injectable } from '@angular/core';
import { TimeService } from './time.service';


@Injectable()
export class WeatherService {

  id: number;
  status: string;
  weatherid: string;
  weatherstatus: string;
  when: string;

  constructor(timeService:TimeService) {
    this.when = timeService.getTime();
  }

  makeWeatherString() {
    this.id = 2;
    this.status = 'Regen';
    this.weatherid = 'ID: ' + this.id.toString() ;
    this.weatherstatus = 'Wetter: ' + this.status;
  }

  getWeatherId() {
    return this.weatherid;
  }

  getWeatherStatus() {
    return this.weatherstatus;
  }

  getWeatherDate() {
    return this.when;
  }
}
