# ForwardApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## For what is this project ?
This project is made to find good solution for circular dependencys between services.

## If you got a solution feel free to contact me or commit it !
contact: `dev@nils-schumacher.com`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
